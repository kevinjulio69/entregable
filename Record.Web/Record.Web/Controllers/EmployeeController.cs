﻿using Microsoft.AspNetCore.Mvc;
using Record.Application;
using Record.Domain.Dtos;
using System;
using System.Threading.Tasks;

namespace Record.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeServices _services;
        private readonly IGetSelectsServices _getSelects;
        public EmployeeController(IEmployeeServices services , IGetSelectsServices getSelects)
        {
            _services = services;
            _getSelects = getSelects;
           
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                return View(await _services.GetEmploye());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
           
        }
        public async Task<IActionResult> Create()
        {
            try
            {
                EmployeeDtos employeeDtos = new EmployeeDtos
                {
                    Areas = await _getSelects.GetArea(),
                    IdentificationType = await _getSelects.GetIdentificationType(),
                    Country = await _getSelects.GetCountry()
                };
                return View(employeeDtos);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
           
        }
        public async Task<IActionResult> Edit(Guid id)
        {
            try
            {
                var employeeDtos = await _services.GetEmployeById(id);
                if (employeeDtos == null)
                    throw new ArgumentNullException(nameof(employeeDtos));

                employeeDtos.IdentificationType = await _getSelects.GetIdentificationType();
                employeeDtos.Areas = await _getSelects.GetArea();
                employeeDtos.Country = await _getSelects.GetCountry();

                return View(employeeDtos);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            

        }

        [HttpPost]
        public async Task<IActionResult> Edit(EmployeeDtos entity)
        {
            try
            {
                var updateEmployeAsync = await _services.UpdateEmployeAsync(entity);
                if (updateEmployeAsync == null)
                    throw new ArgumentNullException(nameof(updateEmployeAsync));
                return Redirect("Index");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> Create(EmployeeDtos entity)
        {
            try
            {
                await _services.AddEmployeAsync(entity);
                return Redirect("Index");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
         
        }

    }
}
