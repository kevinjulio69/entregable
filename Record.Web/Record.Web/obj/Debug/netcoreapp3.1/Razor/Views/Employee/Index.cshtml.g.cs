#pragma checksum "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a3f17715a67c38dd3c61feddc5d2c517da5367d2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Employee_Index), @"mvc.1.0.view", @"/Views/Employee/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\_ViewImports.cshtml"
using Record.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\_ViewImports.cshtml"
using Record.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a3f17715a67c38dd3c61feddc5d2c517da5367d2", @"/Views/Employee/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"aea0d9fa72672da9b7ca380823f857632eec1ba0", @"/Views/_ViewImports.cshtml")]
    public class Views_Employee_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Record.Domain.Dtos.EmployeeIndexDto>>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(" ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a3f17715a67c38dd3c61feddc5d2c517da5367d23324", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
     
     ViewData["Title"] = "Index";
     Layout = "~/Views/Shared/_Layout.cshtml";
   

#line default
#line hidden
#nullable disable
                WriteLiteral(" ");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral(@"
    <script type=""text/javascript"">
        $(document).ready(function ()
        {
            $('#tableEmployee').DataTable();
        });

        $('button#valuedata').click(function ()
        {
            var url = 'https://localhost:44357' + $(this).attr(""value"");
            $('#modalBody').load(url);
        });
        function ShowCreate()
        {
            var create = 'https://localhost:44357/Employee/Create';
            $('#modalBody').load(create);
        }
       
    </script>


");
            }
            );
            WriteLiteral("<style>\r\n.container \r\n{\r\n  display: table  !important;\r\n}\r\n</style>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a3f17715a67c38dd3c61feddc5d2c517da5367d25366", async() => {
                WriteLiteral(@"
    <br />
    <br />
    <button type=""button"" class=""btn btn-outline-success"" data-toggle=""modal"" data-target=""#exampleModal"" onclick=""ShowCreate()"">
       Crear nuevo cliente
    </button>
    <br />
    <br />
    <br />
    <table id=""tableEmployee"" class=""table table-striped table-bordered"">
        <thead>
            <tr>
                <th scope=""col"" style=""display:none"">#</th>
                <th scope=""col"">Tipo de Id</th>
                <th scope=""col"">Documento</th>
                <th scope=""col"">Apellidos</th>
                <th scope=""col"">Nombres</th>
                <th scope=""col"">Pais</th>
                <th scope=""col"">Correo</th>
                <th scope=""col"">Dominio</th>
                <th scope=""col"">Area</th>
                <th scope=""col"">Fecha Admision</th>
                <th scope=""col"">Fecha Creación</th>
                <th scope=""col"">Fecha Modificación</th>
                <th scope=""col"">Opciones</th>
            </tr>
        </thead>
  ");
                WriteLiteral("      <tbody>\r\n");
#nullable restore
#line 65 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("            <tr>\r\n                <td style=\"display:none\">\r\n                    ");
#nullable restore
#line 69 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Id));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 72 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.IdentificationTypesText));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 75 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.IdentificationNumber));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 78 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Fullsurnames));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 81 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Fullname));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 84 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.CountryText));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 87 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Email));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 90 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Domain));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 93 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.AreaText));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 96 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Admissiondate));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 99 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.CreationDate));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                </td>\r\n                <td>\r\n");
#nullable restore
#line 102 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
                     if (item.UpdateDate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        

#line default
#line hidden
#nullable disable
#nullable restore
#line 104 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => string.Empty));

#line default
#line hidden
#nullable disable
#nullable restore
#line 104 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
                                                                   
                    }
                    else
                    {
                        

#line default
#line hidden
#nullable disable
#nullable restore
#line 108 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.UpdateDate));

#line default
#line hidden
#nullable disable
#nullable restore
#line 108 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
                                                                      
                    }

#line default
#line hidden
#nullable disable
                WriteLiteral("                </td>\r\n                <td class=\"Class_td\" scope=\"col\">\r\n                    <button type=\"button\" id=\"valuedata\"");
                BeginWriteAttribute("value", " value=\"", 3687, "\"", 3722, 2);
                WriteAttributeValue("", 3695, "/Employee/Edit/?id=", 3695, 19, true);
#nullable restore
#line 112 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
WriteAttributeValue("", 3714, item.Id, 3714, 8, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n                        Actualizar\r\n                    </button>\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 117 "C:\Users\KEVIN JULIO\Desktop\entregable\entregable\Record.Web\Record.Web\Views\Employee\Index.cshtml"
            }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"        </tbody>
    </table>
    <div class=""modal fade"" id=""exampleModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
        <div class=""modal-dialog"" role=""document"" style=""  max-width: 1000px"">
            <div class=""modal-content"">
                <div class=""modal-header"">
                    <h5 class=""modal-title"" id=""exampleModalLabel"" style=""font-weight:800;"">Operaciones de empleado</h5>
                    <hr style="" background-color: #4349ff;height: 2px;width: 68%;margin-top: 39px;margin-left: -26%;"">
                    <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                        <span aria-hidden=""true"">&times;</span>
                    </button>
                </div>

                <div class=""modal-body"" id=""modalBody"">
                    ...
                </div>
            </div>
        </div>
    </div>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Record.Domain.Dtos.EmployeeIndexDto>> Html { get; private set; }
    }
}
#pragma warning restore 1591
