﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Record.Infrastructure.Context;

namespace Record.Web.Configurations
{
    public static class ContexConfig
    {

        public static void Setup(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<RecordContex>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }
    }
}
