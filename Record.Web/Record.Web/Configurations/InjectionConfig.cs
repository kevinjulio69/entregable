﻿using Microsoft.Extensions.DependencyInjection;
using Record.Infrastructure.Injection;
using System;

namespace Record.Web.Configurations
{
    public static class InjectionConfig
    {

        public static void InjectionSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            DependencyInjection.RegisterServices(services);

        }
    }
}
