﻿using Microsoft.Extensions.DependencyInjection;
using Record.Domain.IGeneric;
using Record.Infrastructure.Generic;

namespace Record.Web.Configurations
{
    public static class Ioc
    {
        public static IServiceCollection AddDependency(this IServiceCollection services)
        {

            services.AddScoped<IUnitContext, UnitContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            return services;
        }
    }
}
