﻿using Microsoft.Extensions.DependencyInjection;
using System;


namespace Record.Web.Configurations
{
    public static class MapperConfig
    {

        public static void AutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }
    }
}
