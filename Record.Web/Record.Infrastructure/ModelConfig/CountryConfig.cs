﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Record.Domain.Models;

namespace Record.Infrastructure.ModelConfig
{
    public class CountryConfig  : IEntityTypeConfiguration<Country>
    {
    public void Configure(EntityTypeBuilder<Country> builder)
    {
        builder.HasKey(e => e.Id);


        builder.Property(e => e.Description)
             .IsRequired()
             .HasMaxLength(100)
             .IsUnicode(false);
      }
    }
}
