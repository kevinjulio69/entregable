﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Record.Domain.Models;

namespace Record.Infrastructure.ModelConfig
{
    public class EmployeeConfig : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(e => e.Id);


            builder.Property(e => e.IdentificationNumber)
                 .IsRequired()
                 .HasMaxLength(50)
                 .IsUnicode(false);

            builder.Property(e => e.Surname)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.SecondSurname)
               .IsRequired()
               .HasMaxLength(50)
               .IsUnicode(false);

            builder.Property(e => e.FirstName)
             .IsRequired()
             .HasMaxLength(50)
             .IsUnicode(false);

            builder.Property(e => e.OtherNames)
             .IsRequired()
             .HasMaxLength(50)
             .IsUnicode(false);

            builder.Property(e => e.Domain)
           .IsRequired()
           .HasMaxLength(50)
           .IsUnicode(false);

            builder.Property(e => e.Email)
           .IsRequired()
           .HasMaxLength(300)
           .IsUnicode(false);

            builder.HasOne(d => d.Countrys)
               .WithMany(p => p.Employees)
               .HasForeignKey(d => d.CountryId)
              .OnDelete(DeleteBehavior.Restrict)
              .HasConstraintName("FK_Employees_Countrys");

            builder.HasOne(d => d.IdentificationTypes)
             .WithMany(p => p.Employees)
             .HasForeignKey(d => d.IdentificationTypesId)
            .OnDelete(DeleteBehavior.Restrict)
            .HasConstraintName("FK_Employees_IdentificationTypes");

            builder.HasOne(d => d.Areas)
            .WithMany(p => p.Employees)
            .HasForeignKey(d => d.AreaId)
           .OnDelete(DeleteBehavior.Restrict)
           .HasConstraintName("FK_Employees_Areas");

            builder.Property(e => e.CreationDate)
              .HasColumnType("datetime")
              .HasDefaultValueSql("(getdate())");
        }
    }

}

