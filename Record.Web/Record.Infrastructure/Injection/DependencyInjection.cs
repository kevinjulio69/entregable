﻿using Microsoft.Extensions.DependencyInjection;
using Record.Application;

namespace Record.Infrastructure.Injection
{
    public class DependencyInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<IEmployeeServices, EmployeeServices>();
            services.AddTransient<IGetSelectsServices,GetSelectsServices>();
            
        }
    }
}
