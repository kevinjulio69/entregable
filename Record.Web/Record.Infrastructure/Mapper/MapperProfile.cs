﻿using AutoMapper;
using Record.Domain.Dtos;
using Record.Domain.Models;

namespace Record.Infrastructure.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Employee, EmployeeDtos>();
            CreateMap<EmployeeDtos, Employee>();
        }
    }
}
