﻿using Microsoft.EntityFrameworkCore;
using Record.Domain.Models;
using Record.Infrastructure.ModelConfig;

namespace Record.Infrastructure.Context
{
    public class RecordContex : DbContext
    {
        public RecordContex(DbContextOptions options) : base(options) { }
        public RecordContex() { }
        public DbSet<Country> Country { get; set; }
        public DbSet<IdentificationType> IdentificationType { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<Employee> Employee { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=CIDE;MultipleActiveResultSets=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CountryConfig());
            modelBuilder.ApplyConfiguration(new IdentificationTypeConfig());
            modelBuilder.ApplyConfiguration(new AreaConfig());
            modelBuilder.ApplyConfiguration(new EmployeeConfig());
            base.OnModelCreating(modelBuilder);
        }
    }
}
