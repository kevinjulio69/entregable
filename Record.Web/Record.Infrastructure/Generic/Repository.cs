﻿using Microsoft.EntityFrameworkCore;
using Record.Domain.Dtos;
using Record.Domain.IGeneric;
using Record.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Record.Infrastructure.Generic
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IUnitContext _unitcontext;

        #region Generic
        public Repository(IUnitContext unitContext)
        {
            _unitcontext = unitContext;
        }
        public async Task<T> CreateAsync(T entity)
        {
            await _unitcontext.Context.Set<T>().AddAsync(entity);
            _unitcontext.Commit();
            return entity;
        }
        public async Task<List<T>> GetSelects()
        {
            return await _unitcontext.Context.Set<T>().ToListAsync();
        }
        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _unitcontext.Context.Set<T>().FindAsync(id);
        }

        public async Task<T> UpdateAsync(T entity)
        {
             _unitcontext.Context.Entry(entity).State = EntityState.Modified;
            _unitcontext.Commit();
            return entity;
        }

        public async Task<T> DeleteAsync(T entity)
        {
            _unitcontext.Context.Entry(entity).State = EntityState.Deleted;
            _unitcontext.Commit();
            return entity;
        }
        #endregion

        #region NotGeneric
        public async Task<IEnumerable<EmployeeIndexDto>> GetAllemployeeByIdAsync()
        {
            var getEmployeeIndexDtos = await (_unitcontext.Context.Employee.Join(_unitcontext.Context.Country,
                    a => a.CountryId,
                    b => b.Id,
                    (a,
                        b) => new { a, b })
                .Join(_unitcontext.Context.Area,
                    @t => @t.a.AreaId,
                    c => c.Id,
                    (@t,
                        c) => new { @t, c })
                .Join(_unitcontext.Context.IdentificationType,
                    @t => @t.@t.a.IdentificationTypesId,
                    d => d.Id,
                    (@t,
                        d) => new EmployeeIndexDto
                    {
                        Id = @t.@t.a.Id,
                        IdentificationNumber = @t.@t.a.IdentificationNumber,
                        AreaText = @t.c.Description,
                        AreaId = @t.@t.a.AreaId,
                        CountryText = @t.@t.b.Description,
                        CountryId = @t.@t.a.CountryId,
                        IdentificationTypesText = d.Description,
                        IdentificationTypesId = @t.@t.a.IdentificationTypesId,
                        Email = @t.@t.a.Email,
                        Fullname = $"{@t.@t.a.FirstName} {@t.@t.a.OtherNames}",
                        Fullsurnames =$"{@t.@t.a.Surname} {@t.@t.a.SecondSurname}",
                        Admissiondate = @t.@t.a.Admissiondate,
                        Domain = @t.@t.a.Domain,
                        CreationDate = @t.@t.a.CreationDate,
                        UpdateDate = @t.@t.a.UpdateDate
                        })).ToListAsync();

            if (getEmployeeIndexDtos == null)
                throw new ArgumentNullException(nameof(getEmployeeIndexDtos));

            return getEmployeeIndexDtos;


           
        }
        public async Task<Employee> GetBytestIdAsync(string email)
        {
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            return await _unitcontext.Context.Employee.Where(x => x.Email.Contains(email))
                .OrderByDescending(x => x.CreationDate)
                .Take(1)
                .FirstOrDefaultAsync();
        }
        public int ValidateDocumentoExists(string documentNumber , int typedocument )
        {
            return  _unitcontext.Context.Employee.Count(x => x.IdentificationTypesId == typedocument && x.IdentificationNumber == documentNumber);
        }
        #endregion

    }
}
