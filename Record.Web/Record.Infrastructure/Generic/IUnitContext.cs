﻿using Record.Infrastructure.Context;
using System;

namespace Record.Infrastructure.Generic
{
    public interface IUnitContext : IDisposable
    {
        RecordContex Context { get; }
        void Commit();
    }
}
