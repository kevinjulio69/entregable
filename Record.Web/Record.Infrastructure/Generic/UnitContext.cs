﻿using Record.Infrastructure.Context;

namespace Record.Infrastructure.Generic
{
    public class UnitContext : IUnitContext
    {
        public RecordContex Context { get; }

        public UnitContext(RecordContex context)
        {
            Context = context;
        }

        public void Commit()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
