﻿using System.Collections.Generic;

namespace Record.Domain.Models
{
    public class Area
    {
        public Area()
        {
            Employees = new HashSet<Employee>();
        }
        public int Id { get; set; }
        public string Description { get; set; }


        public virtual ICollection<Employee> Employees { get; set; }
    }
}
