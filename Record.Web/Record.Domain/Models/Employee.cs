﻿using System;

namespace Record.Domain.Models
{
    public class Employee
    {
        public Guid Id { get; set; }
        public int IdentificationTypesId { get; set; }
        public string IdentificationNumber { get; set; }
        public string Surname { get; set; }
        public string SecondSurname { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public int CountryId { get; set; }
        public string Email { get; set; }
        public string Domain { get; set; }
        public DateTime Admissiondate { get; set; }
        public int AreaId { get; set; }
        public bool Isctive { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public virtual Country Countrys { get; set; }
        public virtual IdentificationType IdentificationTypes { get; set; }
        public virtual Area Areas { get; set; }
    }

}
