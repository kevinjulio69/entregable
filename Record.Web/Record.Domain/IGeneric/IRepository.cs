﻿using Record.Domain.Dtos;
using Record.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Record.Domain.IGeneric
{
    public interface IRepository<T> where T : class
    {
        Task<T> CreateAsync(T entity);
        Task<List<T>> GetSelects();
        Task<T> GetByIdAsync(Guid id);
        Task<T> UpdateAsync(T entity);
        Task<Employee> GetBytestIdAsync(string email);
        Task<IEnumerable<EmployeeIndexDto>> GetAllemployeeByIdAsync();

        int ValidateDocumentoExists(string documentNumber, int typedocument);
        Task<T> DeleteAsync(T entity);
    }
}
