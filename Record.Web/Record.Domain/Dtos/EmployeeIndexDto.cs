﻿using System;

namespace Record.Domain.Dtos
{
    public  class EmployeeIndexDto
    {
        public Guid Id { get; set; }
        public int IdentificationTypesId { get; set; }
        public string IdentificationTypesText { get; set; }
        public string IdentificationNumber { get; set; }
        public string Fullname { get; set; }
        public string Fullsurnames { get; set; }
        public int CountryId { get; set; }
        public string CountryText { get; set; }
        public string Email { get; set; }
        public string Domain { get; set; }
        public DateTime Admissiondate { get; set; }
        public int AreaId { get; set; }
        public string AreaText { get; set; }
        public bool Isctive { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
