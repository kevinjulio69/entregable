﻿using Record.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Record.Domain.Dtos
{
    public class EmployeeDtos
    {
        public Guid Id { get; set; }
        public EmployeeDtos() 
        {
            Id = Guid.NewGuid();
        }
        public int IdentificationTypesId { get; set; }
        public string IdentificationNumber { get; set; }
        public string Surname { get; set; }
        public string SecondSurname { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public int CountryId { get; set; }
        public string Email { get; set; }
        public string Domain { get; set; }
        public DateTime Admissiondate { get; set; }
        public int AreaId { get; set; }



        public List<IdentificationType> IdentificationType { get; set; }
        public List<Area> Areas { get; set; }
        public List<Country> Country { get; set; }

    }
}
