﻿using Record.Domain.Dtos;
using Record.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Record.Application
{
    public interface IGetSelectsServices
    {
        Task<List<Area>> GetArea();
        Task<List<Country>> GetCountry();
        Task<List<IdentificationType>> GetIdentificationType();
    }
}
