﻿using AutoMapper;
using Record.Domain.IGeneric;
using Record.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Record.Application
{
    public class GetSelectsServices : IGetSelectsServices
    {
        private readonly IRepository<Area> _selectArea;
        private readonly IRepository<Country> _selectCountry;
        private readonly IRepository<IdentificationType> _selectIdentificationType;
        private readonly IMapper _imapper;
        public GetSelectsServices(IRepository<Area> selectArea, IRepository<Country> selectCountry, 
                                  IRepository<IdentificationType> selectIdentificationType, IMapper imapper)
        {
            _selectArea = selectArea;
            _selectCountry = selectCountry;
            _selectIdentificationType = selectIdentificationType;
            _imapper = imapper;
        }
        public async Task<List<Area>> GetArea()
        {
            var resultArea = await _selectArea.GetSelects();
            return _imapper.Map<List<Area>>(resultArea);
        }
        public async Task<List<Country>> GetCountry()
        {
            var resultCountry = await _selectCountry.GetSelects();
            return _imapper.Map<List<Country>>(resultCountry);
        }

        public async Task<List<IdentificationType>> GetIdentificationType()
        {
            var resultselectCountry = await _selectIdentificationType.GetSelects();
            return _imapper.Map<List<IdentificationType>>(resultselectCountry);
        }
    }
}
