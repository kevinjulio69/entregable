﻿using Record.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Record.Application
{
    public interface IEmployeeServices
    {
         Task<EmployeeDtos> AddEmployeAsync(EmployeeDtos entity);

         Task<IEnumerable<EmployeeIndexDto>> GetEmploye();

         Task<EmployeeDtos> GetEmployeById(Guid id);

         Task<EmployeeDtos> UpdateEmployeAsync(EmployeeDtos entitys);
         Task<EmployeeDtos> GetEmployeByemails(string email);

         Task<EmployeeDtos> DeleteAsynct(EmployeeDtos entity);
    }
}
