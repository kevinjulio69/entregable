﻿using AutoMapper;
using Record.Domain.Dtos;
using Record.Domain.IGeneric;
using Record.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Record.Application
{
    public class EmployeeServices : IEmployeeServices
    {
        private readonly IRepository<Employee> _employee;
        private readonly IMapper _imapper;


        public EmployeeServices(IRepository<Employee> employee, IMapper imapper)
        {
            _employee = employee;
            _imapper = imapper;
        }

        public async Task<EmployeeDtos> AddEmployeAsync(EmployeeDtos entity)
        {
            EmployeeDtos returnDtos = new EmployeeDtos();
            try
            {
                if (_employee.ValidateDocumentoExists(entity.IdentificationNumber, entity.IdentificationTypesId) == 0)
                {
                    var mapper = _imapper.Map<Employee>(entity);

                    mapper.Domain = entity.CountryId == 1 ? $"cidenet.com.co" : $"cidenet.com.us";

                    var generateEmail = await OperationEmail($"{mapper.FirstName.Replace(" ", "")}.{mapper.Surname.Replace(" ", "")}", mapper.Domain);

                    mapper.Email = generateEmail;
                    mapper.Isctive = true;
                    var addemEmployeeasync = await _employee.CreateAsync(mapper);

                    if (addemEmployeeasync == null)
                        throw new ArgumentNullException(nameof(addemEmployeeasync));

                    returnDtos = _imapper.Map<EmployeeDtos>(addemEmployeeasync);
                }

                return returnDtos;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public async Task<IEnumerable<EmployeeIndexDto>> GetEmploye()
        {
            try
            {
                var result = await _employee.GetAllemployeeByIdAsync();

                if (result == null)
                    throw new ArgumentNullException(nameof(result));

                return _imapper.Map<IEnumerable<EmployeeIndexDto>>(result);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
           
        }

        public async Task<EmployeeDtos> GetEmployeById(Guid id)
        {
            try
            {
                var result = await _employee.GetByIdAsync(id);
                if (result == null)
                    throw new ArgumentNullException(nameof(result));

                return _imapper.Map<EmployeeDtos>(result);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
          
        }

       public async Task<EmployeeDtos> GetEmployeByemails(string email)
        {
            try
            {
                var result = await _employee.GetBytestIdAsync(email);

                if (result == null)
                    throw new ArgumentNullException(nameof(result));
                return _imapper.Map<EmployeeDtos>(result);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
          
        }

        public async Task<string> OperationEmail(string email,string domain)
        {
            try
            {
                string returnString = string.Empty;

                var result = await _employee.GetBytestIdAsync(email);

                if (result?.Email != null && result != null )
                {
                    var retString = result.Email.Substring(0,
                            result.Email.IndexOf("@",
                                StringComparison.Ordinal))
                        .Split('.');
                    switch (retString.Count())
                    {
                        case 2:
                            string[] retString2 = result.Email.Substring(0,
                                    result.Email.IndexOf("@",
                                        StringComparison.Ordinal))
                                .Split('.');
                            returnString = $"{retString2[0]}.{retString2[1]}.1@{domain}".ToLower();
                            break;
                        case 3:

                            string[] retString3 = result.Email.Substring(0,
                                    result.Email.IndexOf("@",
                                        StringComparison.Ordinal))
                                .Split('.');
                            int increase = Convert.ToInt32(retString3[2]) + 1;
                            returnString = $"{retString3[0]}.{retString3[1]}.{increase}@{domain}".ToLower();
                            break;
                    }
                }
                else
                {
                    returnString = $"{email}@{domain}".ToLower();
                }
                return returnString;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
           


          
        }

        public async Task<EmployeeDtos> UpdateEmployeAsync(EmployeeDtos entitys)
        {
            try
            {
                var entity = await _employee.GetByIdAsync(entitys.Id);
                _imapper.Map(entitys, entity);

                entity.Domain = entity.CountryId == 1 ? $"cidenet.com.co" : $"cidenet.com.us";

                var generateEmail = await OperationEmail($"{entity.FirstName.Replace(" ", "")}.{entity.Surname.Replace(" ", "")}", entity.Domain);
                entity.Email = generateEmail;
                entity.UpdateDate = DateTime.Now;
                var status = await _employee.UpdateAsync(entity);
                return _imapper.Map<EmployeeDtos>(status);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }
        public async Task<EmployeeDtos> DeleteAsynct(EmployeeDtos entity)
        {
            try
            {
                var getInformation = await _employee.GetByIdAsync(entity.Id);
                var deleteAsync = await _employee.DeleteAsync(getInformation);
                return _imapper.Map<EmployeeDtos>(deleteAsync);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
    }
}
